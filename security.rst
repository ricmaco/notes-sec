=====================
Sicurezza informatica
=====================

Architettura Intel
==================

x86
---

- 8 registri a 32 bit (EAX, EBX, ECX, EDX)
- 6 registri a 16 bit (AX, BX, CX, DX)
- registri EFLAGS
- registro EIP (*extended instruction pointer*)

x86_64
------

Vengono aggiunti 16 registri a 64 bit (RAX, RBX, RCX, RDX).

Segment registers
-----------------

CS
  punta alla zona di memoria dove risiede il codice in esecuzione.
  Per ottenere l'indirizzo reale bisogna somma ``EIP + CS``.
DS
  punta alla zona di memoria dove risiedono i dati.
SS
  punta alla zona di memoria dove risiede lo stack.
ES, FS, GS
  vengono utilizzati dai compilatori per ottimizzare il processo.

Sono tutti del tipo:

+-----------------+-----------------------------+
| 16 bit visibili | 64 bit invisibili           |
+-----------------+-----------------------------+

Altri registri
--------------

EFLAGS
  indicatori sullo stato del processo corrente.

GNU Assembly
============

L'assembly che verrà usato in questo corso è quello di riferimento
nell'ambiente della sicurezza, **GNU Assembly**, detto *GAS*.

Tipi di dati
------------

- byte (b, 1 byte))
- word (w, 2 byte)
- long word (l, 4 byte)
- quad word (q, 8 byte)

Istruzioni
----------

``[instr] + {b|w|l|q}``

es.

.. code-block:: assembly

  addl $1, %eax

Variabili
---------

- byte: ``.byte``, ``.int``, ``.long``, ``.quad``
- string: ``.ascii``, ``.asciz``

es.

.. code-block:: assembly

  dieci: .ascii "10" -----> [0011][0001][0011][0000]
  ten: .byte 10      -----> [0000][1010]


.. warning:: Little Endian ("*little end goes first*")

  ``0x44332211`` viene memorizzato come [00][11][22][33][44][00][00][00]

Operazioni
----------

``[prefix] opcode operands``

Gli operandi possono essere singoli, ``opcode src``, o doppi, ``opcode src,
dest``.

Dati
----

Ci sono due posti dove memorizzare i dati, i **registri** o le **variabili**.
I registri stanno nell'ALU, mentre le variabili in memoria centrale.

Si usa l'istruzione ``move`` per effettuare load/store.

es

.. code-block:: assembly

  movl %edx, %ecx # muove i 32 bit
  movq %rdx, %rcx # muove i 64 bit

Indirizzamento
--------------

Immediate addressing
  
  .. code-block:: assembly
    
    movl $12, %eax

Direct addressing
  
  .. code-block:: assembly
    
    movl 2000, %eax

Indirect addressing
  
  .. code-block:: assembly
    
    movl (%eax), %ecx

Base pointer addressing

  .. code-block:: assembly
    
    movl 8(%eax), %ecx

Indexed addressing
  
  .. code-block:: assembly
    
      movl $0, %eax
      movl $0, %ebx
    sumloop:
      movl a(,%eax,4), %ecx # indexed addressing
      addl %ecx, %ebx
      incl %eax
      cmpl $19, $eax
      jle sumloop

Operatori
---------

Artimetici
  ``add``, ``sub``, ``inc``, ``dec``, ``neg``.

Moltiplicazione e divisione
  ``mul``/``div`` (o i loro corrispondenti interi, ``imul`` e ``idiv``).

Logici
  ``and``, ``or``, ``not``, etc.

Controllo di flusso
  ``cmp``, ``je``, ``jle``, etc, ``jmp``.

System call
-----------

.. code-block:: assembly

  movl $3, %eax # read syscall
  movl $0, %ebx # stdin
  movl $string, %ecx # buffer
  movl $len, %edx # length
  int 80h # or int $0x80

Note
>>>>

====== =======
Number Syscall
====== =======
1      exit
2      fork
3      read
4      write
5      open
6      close
====== =======

Hello, world!
-------------

.. code-block:: assembly

  .text # sezione testo
  .global _start # label di start

  _start:
    movl $len, %edx
    movl $msg, %ecx
    movl $1, %ebx
    movl $4, %eax
    int $0x80

    movl $0, %ebx
    movl $1, %eax
    int $0x80

  .data # sezione dati
    
  msg:
    .ascii "Hello, world!" # declare a string var
    len = . - msg

Dall'assembly all'eseguibile
============================

Ci sono tre passaggi da eseguire per passare da un programma scritto in
assembly ad un eseguibile: assemble, link e load.

Ovviamente ci sono tre programmi che si occupano di eseguire questi compiti:

assembler
  ``as name.s -o name.o``

linker
  ``ld name.o -o name``

loader
  ``./name``

Per effettuare quello che si chiama un hex dump del programma assemblato si usa
``objdump -h hello.o``.

ELF
===

Struttura interna
-----------------

+----------------------+
| ELF header           |
+----------------------+
| Program header table |
+----------------------+
| .text                |
+----------------------+
| .rodata              |
+----------------------+
| ...                  |
+----------------------+
| .data                |
+----------------------+
| Section header table |
+----------------------+

ReadELF
-------

Utility per avere informazioni su un ELF.
Tramite l'opzione ``-R`` dà una specifica sulle sezioni.

Sezioni
-------

.data
  sezione riservata ai dati inizializzati
.rodata
  sezione riservata ai dati in sola lettura
.bss
  sezione riservata ai dati non inizializzati

GDB
===

Si tratta del GNU Debugger, l'utility GNU per effettuare il debug degli
eseguibili.

Debug di un executable
  ``gdb executable``
Debug di un eseguibile con un coredump
  ``gdb executable -c coredump``

Per abilitare il coredump bisogna lanciare il seguente comando:

.. code-block:: bash

  $ ulimit -c unlimited

Istruzioni di GDB
-----------------

Per compilare un programma con supporto ai simboli di debug si procede nel
seguente modo. Con ``gcc`` il comando è:

.. code-block:: bash

  gcc -g -o name name.c

Mentre con assembler è:

.. code-block:: bash

  as --gstabs -o name.o name.s

Comandi
>>>>>>>

``list x, y``
  stampa la lista di istruzioni dalla riga x, per y righe
``print [/x] $VALUE``
  stampa un valore; c'è la possibilità di specificare la codifica: ``/x`` per
  esadecimale, ``/d`` per decimale e ``/o`` per ottale
``info register $REGISTER``
  dà informazioni sul contenuto di un particolare registro
``x [&]$VAR``
  stampa una variabile o il suo puntatore
``info frame``
  dà un insieme di informazioni riguardanti lo stack
``set $VAR = $EXPR``
  creato o setta una variabile ``$VAR`` valutata come l'espressione ``$EXPR``
``break $X``
  crea un breakpoint alla linea ``$X``
``step``
  fa uno step di una sola istruzione dal precedente breakpoint
``continue``
  continua ad eseguire il programma fino al prossimo breakpoint
``run``
  esegue il programma fino ad eventuali breakpoint e nel caso stampa il
  risultato

Gestione della memoria
======================

La struttura della memoria è ben precisa. È possibile controllare la mappa
della memoria di un processo con:

.. code-block:: bash

  $ cat /proc/$PID/maps

Stack
-----

Struttura **LIFO**, gestita con due registri ``%ss`` e ``%sp``, dove ``%ss`` è
l'indirizzo dello stack e ``%sp`` è l'offset.

Ci sono delle istruzioni dedicate per l'uso dello stack, quali ``push`` e
``pop``.

.. code-block:: assembly

  pushl src

  movl src, (%esp)
  subl $4, %esp

  popl dest

  movl (%esp), dest
  addl $4, %esp

Call / Ret
----------

``call`` salva sullo stack l'indirizzo dell'istruzione successiva da eseguire
nel main (``%ra``) e poi fa un ``jump`` all'istruzione della routine.

``ret`` rimette in ``%ra`` l'indirizzo che ``call`` aveva salvato.

.. code-block:: assembly

  call sub1  

  pushl %eip
  jmp sub1

  ret

  pop %eip

I parametri si possono riferire in questo modo ``4(%esp)``, ``8(%esp)``, ecc. e
quindi vanno copiati in senso inverso.

.. code-block:: assembly

  # push parameters in stack (in reverse order)
  pushl $5
  pushl $4
  pushl $5
  # delete parameters in stack (upper %esp)
  addl $12, %esp

``%ebp`` è un riferimento fisso all'interno dello stack, da poter usare come
base.

.. code-block:: assembly

  # prologo (prime 3 istruzioni di ogni subroutine)
  pushl %ebp
  movl %esp, %ebp
  subl LOCAL_BYTES, %esp

  # or
  enter LOCAL_BYTES, 0

.. code-block:: assempbly

  # epilogo (ultime 3 istruzioni di ogni procedura)
  movl %ebp, %esp
  popl %ebp
  ret

  # or
  leave

Stack frame
-----------

È quella porzione di stack che contiene lo storico di una routine.

- Return address (saved EIP)
- Old EBP
- Saved register values
- Local Variables

Smashing the stack
==================

L'attacco di **buffer overflow** è descritto per la prima volta da *Aleph One*
su Phrack.

Esistono diversi attacchi possibili:

- stack overflow
- heap overflow
- format string
- integer overflow

Strategia generale
------------------

Ci si costruisce un programma, detto *shellcode*, che vogliamo mandare in
esecuzione sulla macchina che attacchiamo.

Il problema è mandarlo sulla macchina vittima ed eseguirlo. Quindi si usa il
buffer overflow per iniettare il programma e si modifica il programma vittima
in modo da eseguirlo.
Altro modo sarebbe quello di individuare sulla macchina vittima il codice già
presente in qualche libreria (e ovviamente farlo eseguire).

Modifica del flusso di controllo
--------------------------------

Il trucco in questo caso è modificare il return address in modo da copiare nel
program counter la locazione di memoria del nostro codice da eseguire.

Esempio
-------

.. code-block:: c

  #include <stdio.h>

  int x = 0;

  int function() {
    x = x + 13;
  }

  void main() {
    x = function(x);
    x = 10;
    printf("Value of x = %d\n", x);
  }

Soluzione
>>>>>>>>>

Comandi di ``gdb``:

.. code-block:: gdb

  break 8
  break 13 # leggi indirizzo printf
  r
  set $var=$rbp+8
  set \*(int \*) $var = $INDIRIZZO_PRINTF
  continue

.. TODO

Anatomia di un attacco informatico
==================================

Gli attacchi sono costituiti da **tre** fasi:

#. iniettare il codice di attacco
#. dirottamento del flusso di controllo al codice di attacco
#. esecuzione del codice di attacco

Esistono anche delle strategie di difesa:

- rilevare criticità all'interno del codice (migliore, ma non immediata)
- evitare l'iniezione di codice
- prevenire l'hijacking del control flow
- prevenire l'esecuzione di codice

Ovviamente si interviene su:

- compilatori
- linker
- loader
- runtime

Esistono diverse strategie: uno stack non eseguibile, protezioni a compile time
oppure librerie che controllano l'attacco. Alcune routine del C sono ad altro
rischio, librerie come ``libsafe.h`` forniscono alternative più sicure.

Soluzioni
---------

Perchè lo stack è deve essere eseguibile? Siccome lo stack contiene dati non ha
bisogno di essere eseguibile. **Write or execute**, quindi o si può scrivere o
si può eseguire: la parte di codice è solo eseguibile, quella di stack è solo
scrivibile.

Per ovviare a questo problema si può o settare un bit nella tabella delle pagine
(NX bit) oppure usare una emulazione software di tipo ExecShield (RedHat) oppure
PaX (Debian) oppure ancora ``PT_GNU_STACK`` (ELF header).

Return to Libc
==============

Questo attacco si basa sul caricamento indiretto dello shellcode. Si cerca lo
shellcode in qualche funzione della ``libc``, gli si passano gli argomenti
corretti e questa funzione verrà eseguita tranquillamente permettendo di fare
buffer overflow.

Si usa una chiamata a sistema del tipo ``system()`` che prende un parametro,
cioè l'indirizzo del codice da eseguire. La difficoltà dell'attacco è
predisporre lo stack a come la ``system`` se lo aspetta.

.. code-block:: bash

  int system(const char \*address)

ROP
===

.. TODO

Canary variable
===============

.. TODO

Reverse engineering
===================

Estrarre la conoscenza o il design da qualcosa, senza documentazione o
specifiche.

Perchè?

- studiare l'architettura interna (dal sorgente)
- ricostruire il codice sorgente (dal binario)
- modificare il comportamento di alcuni programmi
- comprendere l'attività di rete nei protocolli proprietari

Esistono tecniche statiche che si basano sul disassembly e tecniche dinamiche
che prevedono l'esecuzione monitorata dell'applicazione.

I formati eseguibili
====================

I file eseguibili moderni sono complessi:

- **ELF** executable and linkable format
- **PE** portable executable

ELF
---

Possono essere:

- relocatable
- executable
- shared object

======
ELF header
======
Program header table
------
Section 1
------
...
------
Section n
------
...
------
Section header table
======

Le sezioni possono essere:

- ``.text`` sezione testo
- ``.data`` sezione dati inizializzati
- ``.bss`` sezione dati non inizializzati
- ``.interp`` 

Symbol table dinamica (funzioni di libreria) e symbol table locale (funzioni
definite nel mio esegubile).

``.plt`` e ``.got`` regolano il linking dinamico delle librerie. Nella ``.plt``
la prima volta viene chiamato il linker e aggiornata la ``.got`` con l'indirizzo
della funzione richiesta. Il linking pul essere *lazy*.

Disassambly
===========

Devo interpretare uno stream di byte in maniera coerente con l'assembly
dell'architettura che uso.
È possibile che nelle architetture con istruzioni a lunghezza variabile (tipo
Intel) il processo di disassembly venga *desincronizzato*.


Tecniche utilizzate
-------------------

- *linear sweep disassembly*: si decodifica un'istruzione dopo l'altra e può
  succedere che dei dati vengano tradotti come istruzioni
- *recursive traversal disassembly*: si parte da un punto, studia le call e
  disassambla le funzioni per ``jump``, quindi i dati non mi danno fastidio;
  questo decade nel momento in cui gli argomenti delle ``jump`` sono calcolati a
  runtime


